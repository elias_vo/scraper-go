package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
)

const page = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html"

type priceMatcher func(s string) (float64, error)

type Product struct {
	Title       string  `json:"title"`
	Size        string  `json:"size"`
	Price       float64 `json:"unit_price"`
	Description string  `json:"description"`
}

type Results struct {
	Products []*Product `json:"results"`
	Total    float64    `json:"total"`
}

var (
	wGroup     sync.WaitGroup
	cliUrls    string
	singleJson bool
)

func init() {
	flag.StringVar(&cliUrls, "url", page, "Comma separated list of urls to process")
	flag.BoolVar(
		&singleJson,
		"s",
		false,
		"use flag to a single JSON object for all urls (default is separate object per url)",
	)
}

func main() {
	flag.Parse()
	output := map[string]Results{}
	for _, url := range strings.Split(cliUrls, ",") {
		url = strings.TrimSpace(url)
		output[url] = GetResults(url)
	}
	if singleJson {
		if out, err := json.MarshalIndent(output, "", "    "); err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
			os.Exit(2)
		} else {
			fmt.Println(string(out))
		}
		os.Exit(0)
	}
	for url, res := range output {
		fmt.Println(url)
		if out, err := json.MarshalIndent(res, "", "    "); err != nil {
			fmt.Fprintln(os.Stderr, err.Error())
		} else {
			fmt.Println(string(out))
		}
	}
}

func GetResults(url string) Results {
	matcher := getPriceMatcher()
	res := Results{}
	//build the slice, so we can determine the size of the channel correctly
	selections := getProducts(url)
	pchan := make(chan *Product, len(selections))
	ch := make(chan string, len(selections))
	for _, s := range selections {
		wGroup.Add(1)
		go getData(s, matcher, pchan, ch)
	}
	// gopl book, and trial and error suggest this is the
	// safest and best approach to avoid deadlock for unbuffered channels
	// we're using buffered chanels, so we don't need the goroutine
	wGroup.Wait()
	close(pchan)
	close(ch)
	//only set size if not error
	for p := range pchan {
		res.Products = append(res.Products, p)
		res.Total += p.Price
	}
	//this channel contains error strings for things that whent wrong wile processing
	//products, hence productErr -> print to STDERR
	for productErr := range ch {
		fmt.Fprintln(os.Stderr, productErr)
	}
	return res
}

var getProducts = func(url string) []*goquery.Selection {
	dom, err := goquery.NewDocument(url)
	if err != nil {
		fmt.Printf("Failed to load document: %s", err.Error())
		os.Exit(1)
	}
	r := []*goquery.Selection{}
	dom.Find(".product").Each(func(i int, s *goquery.Selection) {
		r = append(r, s)
	})
	return r
}

var setSize = func(link string, p *Product, ch chan string) {
	defer wGroup.Done()
	r, err := http.Get(link)
	if err != nil {
		ch <- err.Error()
		return
	}
	//keep reader open, response is io.ReadCloser
	buffer := bytes.Buffer{}
	bytesRead, err := buffer.ReadFrom(r.Body)
	if err != nil {
		ch <- err.Error()
		return
	}
	// ignore error here, we've defaulted description to title already...
	d, e := goquery.NewDocumentFromReader(
		bytes.NewReader(buffer.Bytes()),
	)
	if e == nil {
		p.Description = strings.TrimSpace(
			d.Find(".productText").Text(),
		)
		p.Description = strings.Replace(p.Description, "\t", "", 0)
	}

	p.Size = fmt.Sprintf("%.2fkb", float64(bytesRead)/1024.0)
}

func getPriceMatcher() priceMatcher {
	re := regexp.MustCompile("\\d+\\.\\d+")
	return func(s string) (float64, error) {
		match := re.FindString(s)
		return strconv.ParseFloat(match, 64)
	}
}

var getData = func(selection *goquery.Selection, matcher priceMatcher, pch chan<- *Product, ch chan string) {
	defer wGroup.Done()
	p := &Product{}

	p.Title = strings.TrimSpace(selection.Find(".productInfo").Text())
	p.Description = p.Title // main page doesn't seem to have a description

	if v, err := matcher(selection.Find(".pricePerUnit").First().Text()); err != nil {
		ch <- err.Error()
		return
	} else {
		p.Price = v
	}
	if v, ok := selection.Find(".productInfo a").First().Attr("href"); ok {
		wGroup.Add(1)
		go setSize(v, p, ch)
	} else {
		ch <- "Could not find details link"
	}
	pch <- p
}
