## Sainsbury's product page scraper

The Go implementation was written and tested usign Go 1.5 on Linux. Compile as any other go application:

```bash
go build path/to/scraper.go
# or to install in $GOPATH/bin
go install path/to/scraper.go
```

Remove debug information from the binary as you normally would:

```bash
go build -ldflags '-w' path/to/scraper.go
```

----

Run the application from the command-line:

```bash
./scraper
```

or, if you've added the `$GOPATH/bin` to your `$PATH`, just run `scraper`

The program takes two arguments (run `scraper -h` for a brief description):

* -url: Expects a comma separated list of urls to scrape. Default value is the page given in the assignment
* -s  : use this flag to get the output as a single JSON object (default is one object per page)

If you use the `-s` flag, the output format is unchanged, but the single object is a JSON respresentation of a `map[string]Result{}`, where the string is the URL:

```json
{
    "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html": {
        "results": [
            {
                "title": "the title",
                "size": "12.34kb",
                "unit_price": 1.23,
                "description": "The full description of this item"
            }
        ],
        "total": 1.23
    }
}
```

So basically, to scrape several pages, and get the output in a single file with valid JSON, run:

```bash
scraper -urls=url1,url2,url3 -s > out.json
```

-----

Running the test, either from within the src directory:

```
go test -v
```

or from a different location:

```
go test -v path/to/*.go
```

There currently are 2 tests: one to process valid data only. The test data is expected to be mapped directly onto the result
The other test checks to see whether or not data with an invalid unit price is skipped or not. The implementation uses a channel to send the errors to the stderr, so this will show in the output of the test:

```
=== RUN   TestFullFlowOneResult
--- PASS: TestFullFlowOneResult (0.00s)
=== RUN   TestSkipInvalidProduct
strconv.ParseFloat: parsing "": invalid syntax
--- PASS: TestSkipInvalidProduct (0.00s)
PASS
ok  	sainsbury	0.005s
```

----

Apart from one package, all dependencies are standard go packages. The only exception is the goquery package. Install by running:

```
go get github.com/PuerkitoBio/goquery
```

The reason for using this package is because it offers a good abstraction layer on top of the net/http package and cascadia. It allows you to query the DOM easily using css selectors easily, not unlike JavaScript's `document.querySelector('.some_class')` and more popular JS-based libraries.
