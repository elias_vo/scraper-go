package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"testing"

	"github.com/PuerkitoBio/goquery"
)

type MockSection struct {
	goquery.Selection
	Title       string
	Description string
	Size        string
	PriceStr    string
	used        bool
}

//DRY - We use mock products in the overrides, convert them to expected products here
func mocksToProducts(mocks []*MockSection) []*Product {
	//r := make([]*Product, len(mocks))
	//use append to accomodate error mocks
	r := []*Product{}
	for _, m := range mocks {
		//ignore error, we have control over the actual mock values anyway
		if price, err := strconv.ParseFloat(m.PriceStr, 64); err == nil {
			r = append(r, &Product{
				Price:       price,
				Title:       m.Title,
				Description: m.Description,
				Size:        m.Size,
			})
		}
	}
	return r
}

//helper function to compare results in tests
func getProductMap(products []*Product) map[string]*Product {
	r := map[string]*Product{}
	for k := range products {
		r[products[k].Title] = products[k]
	}
	return r
}

//Because we're using goroutines, the order of results is unpredictable
//use this function to compare product by product instead
func compareResults(expect Results, actual Results) (bool, error) {
	if expect.Total != actual.Total {
		return false, fmt.Errorf(
			"Expected Total to be %f, instead saw %f",
			expect.Total,
			actual.Total,
		)
	}
	if len(expect.Products) != len(actual.Products) {
		return false, fmt.Errorf(
			"Expected %d products, actual: %d",
			len(expect.Products),
			len(actual.Products),
		)
	}
	eMap := getProductMap(expect.Products)
	aMap := getProductMap(actual.Products)
	for title, product := range eMap {
		if aProd, ok := aMap[title]; !ok {
			return false, fmt.Errorf(
				"Did not find expected product %s",
				title,
			)
		} else {
			eJson, _ := json.MarshalIndent(product, "", "    ")
			aJson, _ := json.MarshalIndent(aProd, "", "    ")
			if string(eJson) != string(aJson) {
				return false, fmt.Errorf(
					"JSON mismatch -> Expected %s actual %s",
					string(eJson),
					string(aJson),
				)
			}
		}
	}
	for title := range aMap {
		if _, ok := eMap[title]; !ok {
			return false, fmt.Errorf(
				"Did not expect to see product %s",
				title,
			)
		}
	}
	return true, nil
}

//Test expected input - output, no errors encountered along the way
func TestFullFlowOneResult(t *testing.T) {
	mocks := []*MockSection{
		&MockSection{
			PriceStr:    "12.34",
			Title:       "foobar",
			Size:        "12kb",
			Description: "Some description for foobar",
		},
		&MockSection{
			PriceStr:    "12.12",
			Title:       "something else",
			Size:        "10kb",
			Description: "Description here",
		},
		&MockSection{
			PriceStr:    "1.14",
			Title:       "other",
			Size:        "38.16kb",
			Description: "description of another product",
		},
	}
	currentOffset := 0
	//return nil pointers, as many as there are mock objects
	getProducts = func(url string) []*goquery.Selection {
		return make([]*goquery.Selection, len(mocks))
	}

	setSize = func(link string, p *Product, ch chan string) {
		defer wGroup.Done()
		//ensure we're using the right mock object
		mock := mocks[currentOffset]
		for i := 0; i < len(mocks); i++ {
			if p.Title == mocks[i].Title {
				mock = mocks[i]
				break
			}
		}
		p.Size = mock.Size
		p.Description = mock.Description
	}

	getData = func(selection *goquery.Selection, matcher priceMatcher, pch chan<- *Product, ch chan string) {
		defer wGroup.Done()
		p := &Product{}
		//get the first available mock object
		var mock *MockSection
		for i := range mocks {
			if mocks[i].used == false {
				mock = mocks[i]
				mock.used = true
				break
			}
		}

		p.Title = strings.TrimSpace(mock.Title)
		p.Description = p.Title // main page doesn't seem to have a description

		if v, err := matcher(mock.PriceStr); err != nil {
			ch <- err.Error()
			return
		} else {
			p.Price = v
		}
		wGroup.Add(1)
		go setSize("", p, ch)
		pch <- p
	}

	//process page
	r := GetResults("http://google.co.uk")

	//expected outcome
	expect := Results{
		Products: mocksToProducts(mocks),
		Total:    0,
	}
	for k := range expect.Products {
		expect.Total += expect.Products[k].Price
	}
	pass, err := compareResults(expect, r)
	if pass == false {
		t.Errorf("Test failed when comparing results: %s\n", err.Error())
		expectString, _ := json.MarshalIndent(expect, "", "    ")
		actualString, _ := json.MarshalIndent(r, "", "    ")
		t.Errorf(
			"full output Expects: %s \n--\n Actual: %s\n",
			string(expectString),
			string(actualString),
		)
	}
}

//Test to see if products with unknown price are being skipped
func TestSkipInvalidProduct(t *testing.T) {
	mocks := []*MockSection{
		&MockSection{
			PriceStr:    "nothing",
			Title:       "error",
			Size:        "12kb",
			Description: "Some description for foobar",
		},
		&MockSection{
			PriceStr:    "12.34",
			Title:       "foobar",
			Size:        "12kb",
			Description: "Some description for foobar",
		},
		&MockSection{
			PriceStr:    "1.14",
			Title:       "other",
			Size:        "38.16kb",
			Description: "description of another product",
		},
	}
	currentOffset := 0
	//return nil pointers, as many as there are mock objects
	getProducts = func(url string) []*goquery.Selection {
		return make([]*goquery.Selection, len(mocks))
	}

	setSize = func(link string, p *Product, ch chan string) {
		defer wGroup.Done()
		//ensure we're using the right mock object
		mock := mocks[currentOffset]
		for i := 0; i < len(mocks); i++ {
			if p.Title == mocks[i].Title {
				mock = mocks[i]
				break
			}
		}
		p.Size = mock.Size
		p.Description = mock.Description
	}

	getData = func(selection *goquery.Selection, matcher priceMatcher, pch chan<- *Product, ch chan string) {
		defer wGroup.Done()
		p := &Product{}
		//get the first available mock object
		var mock *MockSection
		for i := range mocks {
			if mocks[i].used == false {
				mock = mocks[i]
				mock.used = true
				break
			}
		}

		p.Title = strings.TrimSpace(mock.Title)
		p.Description = p.Title // main page doesn't seem to have a description

		if v, err := matcher(mock.PriceStr); err != nil {
			ch <- err.Error()
			return
		} else {
			p.Price = v
		}
		wGroup.Add(1)
		go setSize("", p, ch)
		pch <- p
	}

	//process page
	r := GetResults("http://google.co.uk")

	//expected outcome
	expect := Results{
		Products: mocksToProducts(mocks),
		Total:    0,
	}
	for k := range expect.Products {
		expect.Total += expect.Products[k].Price
	}
	pass, err := compareResults(expect, r)
	if pass == false {
		t.Errorf("Test failed when comparing results: %s\n", err.Error())
		expectString, _ := json.MarshalIndent(expect, "", "    ")
		actualString, _ := json.MarshalIndent(r, "", "    ")
		t.Errorf(
			"full output Expects: %s \n--\n Actual: %s\n",
			string(expectString),
			string(actualString),
		)
	}
}
